FROM nginx:alpine

WORKDIR /app

COPY ./index.html .

COPY ./nginx.conf /etc/nginx/nginx.conf
